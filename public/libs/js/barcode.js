
$(document).ready(function(){
	var tmppath;
	$('#code').change( function(event) {
		tmppath = URL.createObjectURL(event.target.files[0]);
		console.log(tmppath);
   	});

	$("#scan").click(function(e)
	{
		e.preventDefault();
		if( tmppath&&tmppath.length){
			$("#scannerForm").css("display", "none");
			$("#loading").css("display", "block");
			$("#code").val("");
			$("#resultWrapper").empty();
			var data = {tmppath:tmppath};
			$.ajax({
			  	type: "POST",
				url: "/barcode",
				data: data,
				success: function(response) {
					$("#loading").css("display", "none");
					$("#scannerForm").css("display", "block");
					var result = response.message;
					result.forEach(function(code){
						var wrapper = "";
						var srcVaue = "/barcodes/GOS 1 Batch Header-page-001.jpg";
						if(code.exactMatch){
							console.log(code.fileName);
							wrapper = "<li class='col-md-12 col-xs-12' style='margin-top:8px;font-size:15px;'><span class='col-md-6 col-xs-6'>"+code.fileName+"</span><span class='col-md-6 col-xs-6'><a download = 'image'><img src='/libs/images/download.png' alt=Download icon' height='40' width='40'></a></span>";
							 $("a").attr("href", srcVaue);
						}else{
							console.log(code.fileName);
							wrapper = "<li class='col-md-12 col-xs-12' style='margin-top:8px;font-size:15px;'><span class='col-md-6 col-xs-6'>"+code.fileName+"</span><span class='col-md-6 col-xs-6'><a download ='image'><img src='/libs/images/download.png' alt=Download icon' height='40' width='40'></a></span></li>";
							$("a").attr("href", srcVaue);
						}
						
						$("#resultWrapper").append(wrapper);
					});
				},
				error: function(err) {
					$("#loading").css("display", "none");
					$("#scannerForm").css("display", "block");
					console.log("failure");
				},
				dataType: "JSON"
			});
			tmppath = "";
		}
		return alert("Please Choose Folder");
	})
})