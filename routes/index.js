const express = require('express');
const router = express.Router();
var scannerController = require("../server/scanner");

router.get('/', function(req, res, next) {
	res.render('index');
});

router.post("/barcode",function(req, res){
	req.connection.setTimeout(600000);
	console.log(req.body.tmppath);
	scannerController.scan(req,res); 
});

module.exports = router;
