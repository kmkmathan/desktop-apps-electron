var fs = require("fs");
var quagga = require('quagga');
var async = require('async');
//var  ScannedReport= require('./scannedReport');

function ScannerController() {};

ScannerController.prototype.scan = function(req,res) {
    async.waterfall([
        scannerPage.bind(this, req),
        ],function(result){
        res.status = 200;
        return res.json({error:null, message: result});
    });
}
var scannerPage= function(req,cb,barcode)
{
    var files = fs.readdirSync("./barcodes");
    console.log(req.body.tmppath);
    console.log(__dirname);
    var count = 0;
    var scope = this;
    var barcodesDetails = [];

    async.whilst(
        function () {
            return count < files.length; 
        },
        function (callback) {
            var path = "./barcodes/"+files[count];
            console.log(path);
            quagga.decodeSingle({
                src: path,
                numOfWorkers: 0,  // Needs to be 0 when used within node
                inputStream: {
                    size: 1920,
                    area : {
                        top : "10%",
                        left : "10%"
                    }
                },
                decoder: {
                    readers: ["code_39_reader"] // List of active readers
                },
            }, function(result) {
                console.log("ffffffff");
                if(result && result.codeResult) {
                    console.log("result", files[count], result.codeResult.code);
                    barcodesDetails.push({fileName : files[count], barcode : result.codeResult.code});
                    count++;
                    callback();
                } else {
                    console.log("not detected", files[count]);
                    var file = {
                        name: files[count],
                        path: './barcodes/'
                    };
                   
                }
            });
            
        },
        function (err, n) {
            var scannedReport = [];
            barcodesDetails.forEach(function(code){
                if(code.barcode === barcode){
                    scannedReport.push({fileName:code.fileName})
                }else if(code.barcode.indexOf(barcode) > -1){
                    scannedReport.push({fileName:code.fileName})
                }else{
                    scannedReport.push({fileName:code.fileName})
                }
            });
            console.log(scannedReport, cb);
            // var data = scannedReport;
            // for(var i in data){
            //     var scannedReportValue = new ScannedReport(data[i]);
            //     scannedReportValue.save();
            //     }   
            return cb(scannedReport);
        }
    );
};

module.exports = new ScannerController();
